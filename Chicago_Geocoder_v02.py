'''
Copyright: Toby Tennent, http://www.spatialml.com
Feb 2019

A rudimentary offline geocoder, tuned for Chicago.

Requirements:
pip install pyspellchecker
pip install pandas


Reference:
http://results.openaddresses.io/
http://www.gis.co.clay.mn.us/usps.htm
https://www.propublica.org/datastore/dataset/chicago-parking-ticket-data

History:
v0.2 -
    added support for reading zipped source ... but loads geocode target file into memory, so be careful
    added counts to common misspells dictionary for analytics

v0.1 -
    added common misspells dictionary heuristic

'''

import os.path
import csv
import datetime as dt
import re
from spellchecker import SpellChecker
from bisect import bisect_left
import pandas as pd
from operator import itemgetter
import zipfile

# Globals

basepath = "C:/Users/toby_/PycharmProjects/Chicago_Geocoder"

if basepath[-1] != "/": basepath = basepath + "/"  # <--- because I always forget

## input file to geocode
#
# input_geocode_file_name = basepath + 'propublica_parking_tickets_sample.csv'
input_geocode_file_name = basepath + 'il_tickets_7428b112.zip'
# 'C:/Users/toby_/PycharmProjects/Chicago_Geocoder/il_tickets_7428b112.zip'
input_geocode_file_name = basepath + 'chicago_parking_tickets.csv'
# if processing from zip file:
zip_internal_file_path = 'data/exports/chicago_parking_tickets.csv'

geocode_file_address_colid = 2

house_number_match_range = 500  # geocode to the closest house number -/+ this value

## Optional input row QA checks
## An input row must match these type checks to be processed. Leave empty if not required
#
intColids = [0, 9]  # <-- expected integer columns
floatColids = [12, 13, 14, 15]  # <-- expected float columns
datetimeColids = [1, 17]  # <-- expected datetime columns
datetimeFormat = '%Y-%m-%d %H:%M:%S'  # <-- expected datetime format

# Do we want to limit the amount of input we process?
max_rows_to_parse = 10000  # <--Set to a really high number if you want the entire file

# do we want to try spell checking
spell_check_1_char = True  # accept 1 character difference?
spell_check_2_char = False  # accept 2 character difference? (computationally expensive)

# output files
good_mm_output_file_name = basepath + 'good_Mar_MM.csv'
bad_mm_output_file_name = basepath + 'bad_Mar_MM.csv'

# this is our 'geocoder look up' file:
openaddr_filename = basepath + 'openaddr_city_of_chicago.csv'
lat_colname = 'LAT'
lon_colname = 'LON'
housenum_colname = 'NUMBER'
street_colname = 'STREET'
city_colname = 'CITY'  # <-- not currently used, blank in openaddr dataset
district_colname = 'DISTRICT'  # <-- not currently used ... maybe never use?
region_colname = 'REGION'  # <-- not currently used
postcode_colname = 'POSTCODE'  # <-- not currently used

# this is a file with a list of abbreviations used by the US Postal Service
usps_abbreviations_filename = basepath + 'usps_address_abbreviations.csv'
fullname_colname = 'fullname'
abbrevation_colname = 'abbreviation'

# load/output the common spelling errors found in previous runs? Leave as a blank string to not.
common_misspells_filename = basepath + 'common_misspells.csv'
reset_misspell_counts = True


#
#
#
#
#

def int_checker(strIn):
    try:
        intOut = int(strIn)
        return True
    except:
        return False


def float_checker(strIn):
    try:
        floatOut = float(strIn)
        return True
    except:
        return False


def datetime_checker(strIn, format):
    try:
        mydate = dt.datetime.strptime(strIn, format)
        return True
    except:
        return False


def make_simple_street_name_list(openaddr_filename, street_colname="street", city_colname="city",
                                 region_colname="region"):
    """
    +++ NEED TO ORDER BY COUNT
    :param openaddr_filename:
    :param street_colname:
    :param city_colname:
    :param region_colname:
    :return:
    """
    df = pd.read_csv(openaddr_filename)

    # order by count for the regex search later
    df_agg = df.groupby(street_colname)[street_colname].count().reset_index(name='count').sort_values('count',
                                                                                                      ascending=False)
    outList = df_agg[street_colname].values.tolist()

    return outList


def make_contraction_dictionary(usps_abbreviations_filename):
    dictOut = {}
    with open(usps_abbreviations_filename, 'r') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in csvreader:
            for i in range(len(row)):
                for j in range(len(row)):
                    if i != j:
                        dictOut[row[i]] = row[j]
    return dictOut


def make_simple_contraction_dictionary(usps_abbreviations_filename):
    dictOut = {}
    with open(usps_abbreviations_filename, 'r') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in csvreader:
            full = row[0].strip()
            contraction = row[1].strip()
            dictOut[contraction] = full
    return dictOut


def make_street_name_dict_with_house_numbers(openaddr_filename, housenum_colname="number", street_colname="street",
                                             city_colname="city", region_colname="region"):
    """
    Note: attempted this with varients of pandas iterations and it was really slow! Switched to straight csv load

    :param openaddr_filename:
    :param housenum_colname:
    :param street_colname:
    :param city_colname:
    :param region_colname:
    :return:
    """

    dictOut = {}
    with open(openaddr_filename, 'r') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        rowCount = 0
        for row in csvreader:
            dictHeader = {}
            if rowCount == 0:
                for i in range(len(row)):
                    dictHeader[row[i]] = i
                street_colid = dictHeader[street_colname]
                housenum_colid = dictHeader[housenum_colname]

            else:
                street = row[street_colid]
                number = row[housenum_colid]

                if street != '':
                    if street in dictOut:
                        listOut = dictOut[street]
                    else:
                        listOut = []
                    listOut.append(int(number))
                    dictOut[street] = listOut
            rowCount += 1

    for k, v in dictOut.items():
        listNums = dictOut[k]
        listNums = sorted(listNums)
        dictOut[k] = listNums

    return dictOut


def make_dictFullAddr_latlon(openaddr_filename, housenum_colname="number", street_colname="street", lat_colname='lat',
                             lon_colname='lon', city_colname="city", region_colname="region"):
    dictOut = {}
    with open(openaddr_filename, 'r') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        rowCount = 0
        for row in csvreader:
            dictHeader = {}
            if rowCount == 0:
                for i in range(len(row)):
                    dictHeader[row[i]] = i
                street_colid = dictHeader[street_colname]
                housenum_colid = dictHeader[housenum_colname]
                lat_colid = dictHeader[lat_colname]
                lon_colid = dictHeader[lon_colname]

            else:
                street = row[street_colid]
                try:
                    number = int(row[housenum_colid])
                except:
                    number = -999
                lat = float(row[lat_colid])
                lon = float(row[lon_colid])

                if street != '':
                    dictOut[str(number) + ' ' + street] = (lat, lon)

            rowCount += 1

    for k, v in dictOut.items():
        listNums = dictOut[k]
        listNums = sorted(listNums)
        dictOut[k] = listNums

    return dictOut


def make_spell_street_name_list(openaddr_filename, dictContractions, street_colname="street", city_colname="city",
                                region_colname="region"):
    """
    Builds a list of street names for spell function.
    Spell uses recurrance of a word (street name in our case) to determine most likely match in case of a typo
    So, we build a list of street name based on number of addresses seen in the openaddr file

    :param openaddr_filename:
    :param dictContractions:
    :param street_colname:
    :param city_colname:
    :param region_colname:
    :return:
    """

    listStreets = []
    listContractions = []

    listExclusions = ['N', 'S', 'E', 'W']

    # get full list
    df = pd.read_csv(openaddr_filename)

    #    df_streets = df.groupby([street_colname, city_colname, region_colname])[street_colname].count()
    df_streets = df.groupby([street_colname])[street_colname].count()

    max_count = -1
    divider = 100

    dictTemp = {}

    for street, count in df_streets.iteritems():

        repeats = int(round(float(count) / float(divider), 0))
        if repeats < 1: repeats = 1

        tempList = street.split(' ')
        newList = []
        for item in tempList:
            if item not in dictContractions:
                newList.append(item)
            else:
                if item not in listExclusions:
                    if item in dictTemp:
                        count = dictTemp[item] + repeats
                    else:
                        count = repeats
                    dictTemp[item] = count

        street_name = (' '.join(newList).strip())
        if street_name != '':
            for i in range(repeats):
                listStreets.append(street_name)

    for k, v in dictTemp.items():
        for i in range(v):
            listContractions.append(k)

    return listStreets, listContractions


def spell_check_street(street_name, spell):
    listExclusions = ['N', 'S', 'E', 'W']
    dictCommonConcats = {'COURT': 'CT', 'DRIVE': 'DR', 'HEIGHTS': 'HTS', 'HIGHWAY': 'HWY',
                         'ROAD': 'RD', 'STREET': 'ST', 'PLACE': 'PL', 'TERRACE': 'TER'}

    listStreet = street_name.split(' ')

    # see if we can truncate the street type (eg 'STREET' to 'ST')
    if listStreet[-1].strip() in dictCommonConcats:
        listStreet[-1] = dictCommonConcats[listStreet[-1]]

    for i in range(len(listStreet)):
        # fix this issue, seems prevalent in Parking Tickets dataset
        if listStreet[i] == 'PK': listStreet[i] = 'PARK'

        # spell correct
        if listStreet[i] not in listExclusions:
            listStreet[i] = spell.correction(listStreet[i])

    street_name = ' '.join(listStreet)

    return street_name


def getClosestNumber(myList, myNumber):
    """
    ref: https://stackoverflow.com/questions/12141150/from-list-of-integers-get-number-closest-to-a-given-value

    Assumes myList is sorted. Returns closest value to myNumber.

    If two numbers are equally close, return the smallest number.
    """
    pos = bisect_left(myList, myNumber)
    if pos == 0:
        return myList[0]
    if pos == len(myList):
        return myList[-1]
    before = myList[pos - 1]
    after = myList[pos]
    if after - myNumber < myNumber - before:
        return after
    else:
        return before


def geocode_address(target_address, acceptable_number_range, dictFound, dictAddrContra,
                    listStreetNames, dictStreetNames_listNums, spell1=None, spell2=None):
    """

    Errors seem to be:
    missing address numbers from openaddr table
    N/S/E/W confusion in the parking tickets test file

    strategy:
    try full address match - Quality 1. Match rate: ... total: 28,270,000 good: 23,409,277 ... bad: 4,860,722
    try to fix spelling errors in street address - Quality 2
    try ranged address match (pick nearest address) - Quality 3
    try contractions, not N/S/E/W removed match - Quality 4
    try all contractions removed match .. Quality 5
    """

    lat = -999
    lon = -999
    matchNum = -999
    matchStreet = 'NOT_FOUND'
    street_address_flag = False
    candidates = []

    # tidy and standardise target address
    target_address = target_address.strip()  # remove leading/trailing spaces

    listAddr = target_address.split()

    for i in range(len(listAddr)): listAddr[i] = re.sub('\W+', '', listAddr[i])  # remove non alpha numeric characters

    # is the first part of the address a house number?
    if listAddr[0].isdigit() == True:
        target_house_number = int(listAddr[0])
        street_address_flag = True
    else:
        target_house_number = -1

    # get street
    if street_address_flag == True:
        start_loc = 1
    else:
        start_loc = 0

    street = ' '.join(listAddr[start_loc:])
    input_street = street.strip()
    full_address = str(target_house_number) + ' ' + street
    quality = 0

    ## Sort out street name
    #
    # see if we can find street name in dictStreetNames_listNums as is

    # see if we've found this mis-spelling street before
    if input_street in dictFound:
        candidates = dictFound[input_street]

    if len(candidates) == 0:
        if input_street in dictStreetNames_listNums:
            quality = 1
            count = 0
            candidates = [[input_street, quality, count]]
        else:
            # try regex search
            regex = re.compile(r'(.*)(' + street + ' )(.*)').search
            candidates = streetNameFilter(listStreetNames, regex)

            if len(candidates) > 0:
                quality = 3
                for i in range(len(candidates)):
                    candidates[i][1] = quality
                    candidates[i][1] += 1
                dictFound[input_street] = candidates

    # if not found, spell check street to 1 letter and try again
    if spell1 != None:
        if len(candidates) == 0:
            street_corrected = spell_check_street(input_street, spell1)

            if street_corrected in dictStreetNames_listNums:
                quality = 5
                count = 0
                candidates = [[street_corrected, quality, count]]
                dictFound[input_street] = candidates

    if spell2 != None:
        # if not found, spell check street to 2 letters and try again
        # +++ Note: This is a costly function
        if len(candidates) == 0:
            street_corrected = spell_check_street(input_street, spell2)

            if street_corrected in dictStreetNames_listNums:
                quality = 7
                count = 0
                candidates = [[street_corrected, quality, count]]
                dictFound[input_street] = candidates

    # if not found truncate street name
    if len(candidates) == 0:
        listStreet = street_corrected.split(' ')
        newstreet = []
        for item in listStreet:
            if item not in dictAddrContra:
                newstreet.append(item)

        # don't need to try direct match this time as we know it won't be there. Jump straight to regex search
        street_truncated = ' '.join(newstreet)
        if len(street_truncated) > 0:
            regex = re.compile(r'(.*)(' + street_truncated + ' )(.*)').search
            candidates = streetNameFilter(listStreetNames, regex)
            if len(candidates) > 0:
                quality = 9
                for i in range(len(candidates)):
                    candidates[i][1] = quality
                # dictFound[input_street] = candidates

    dictFound[input_street] = candidates

    # if we have identified a list of candidate streets, get the closest number to the target address within an acceptable range
    if len(candidates) > 0:
        # +++ Getting some 'object leak' here - need to ensure we don't contaminate the candidate list in dictFound.
        # +++ So - build a brand new list from candidates ... surely there's an easier way to do this?
        candidates_temp = list()
        for i in range(len(candidates)):
            temp = list()
            for j in range(len(candidates[i])):
                temp.append(candidates[i][j])
            if len(temp) < 3:
                while len(temp) < 3:
                    temp.append(0)
            candidates_temp.append(temp)

        for i in range(len(candidates_temp)):
            street = candidates_temp[i][0]
            if street in dictStreetNames_listNums:
                listNums = dictStreetNames_listNums[street]
                closestNum = getClosestNumber(listNums, target_house_number)
                closestNumDiff = (target_house_number - closestNum)
                candidates_temp[i].append(closestNum)
                candidates_temp[i].append(closestNumDiff)

        # now find the closest address to the input:
        sorted_candidates = sorted(candidates_temp, key=itemgetter(3))
        street = sorted_candidates[0][0]
        quality = sorted_candidates[0][1]
        count = sorted_candidates[0][2]
        closestNum = sorted_candidates[0][3]
        closestNumDiff = sorted_candidates[0][4]

        if closestNumDiff <= acceptable_number_range:
            if abs(target_house_number - closestNum) != 0:
                quality += 1
            full_address = str(closestNum) + ' ' + street
        else:
            quality = 0
    else:
        quality = 0

    # if we found a house number within range, go get the lat lon
    if quality > 0:
        if full_address in dictFullAddr_latlon:
            outTuple = dictFullAddr_latlon[full_address]
            lat = outTuple[0]
            lon = outTuple[1]
            matchStreet = street
            matchNum = closestNum
            count += 1

        # add to dictFound count for appropriate record
        temp = dictFound[input_street]
        for i in range(len(temp)):
            if temp[i][0] == matchStreet:
                if len(temp[i]) < 3:
                    temp[i].append(1)
                else:
                    temp[i][2] += 1
            else:
                if len(temp[i]) < 3:
                    temp[i].append(0)
        dictFound[input_street] = temp

    return round(lat, 6), round(lon, 6), matchNum, matchStreet, quality, dictFound


def streetNameFilter(list, filter):
    return [[l, m.group(1)] for l in list for m in (filter(l),) if m]


def streetNameFilter2(regexStreetNames, target_streetname):
    outStreets = re.search(r'(.*)(' + target_streetname + ' )(.*)', regexStreetNames, flags=0)
    return outStreets


def readCommonMisspellsFile(filename):
    '''
    :param filename:
    :return:
    '''
    dictFound = {}
    # try:
    if 1 == 1:
        if os.path.isfile(filename):

            csv_file = open(filename, "r")
            reader = csv.reader(csv_file, delimiter=',', quotechar='"')

            for line in reader:
                k = line[0]
                v = line[1:]
                for i in range(len(v)):
                    if v[i].isnumeric():
                        v[i] = int(v[i])  # need to convert quality & count to integer

                if len(v) < 3:
                    while len(v) < 3:
                        v.append(0)

                if k in dictFound:
                    listTemp = dictFound[k]
                else:
                    listTemp = []
                listTemp.append(v)
                dictFound[k] = listTemp
    # except:
    #    print('Problem with', filename, '... not using preloaded common misspells file ...')

    return dictFound


def writeCommonMisspellsFile(filename, dictFound):
    '''
    Create/overwrite to a file to store common Street name misspells
    Make it human readable so we can easily edit and/or put in a database

    :param filename:
    :param dictFound:
    :return:
    '''
    with open(filename, "w", newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=',', quotechar='"')
        for k, v in dictFound.items():
            for addr in v:
                try:
                    writer.writerow([k, addr[0], str(addr[1]), str(addr[2])])
                except:
                    pass


starttime = dt.datetime.now()
str_starttime = str(starttime)[:19]
print('Begin!', starttime)

# set up output files
bad_writer = open(bad_mm_output_file_name, 'w')
good_writer = open(good_mm_output_file_name, 'w')

# open connection to our geocoder database
# conn = sqlite3.connect(my_sqlite_db)
# curs = conn.cursor()
# print ('... loading', my_sqlite_db, 'in to memory')

print('... loading support files ...')

# get list of unique street names
listStreetNames = make_simple_street_name_list(openaddr_filename, street_colname=street_colname,
                                               city_colname=city_colname, region_colname=region_colname)

# get dict of street names with a sorted list of house numbers
dictStreetNames_listNums = make_street_name_dict_with_house_numbers(openaddr_filename,
                                                                    housenum_colname=housenum_colname,
                                                                    street_colname=street_colname,
                                                                    city_colname=city_colname,
                                                                    region_colname=region_colname)

# get dict of full address with lat lon
dictFullAddr_latlon = make_dictFullAddr_latlon(openaddr_filename, housenum_colname=housenum_colname,
                                               street_colname=street_colname,
                                               lat_colname=lat_colname, lon_colname=lon_colname,
                                               city_colname=city_colname, region_colname=region_colname)

# load our contraction dictionary
dictAddrContra = make_contraction_dictionary(usps_abbreviations_filename)

listContractions = list(dictAddrContra.keys())
# need to get counts of contractions from openaddr table to get spell to work properly
dictAddrContra2 = make_simple_contraction_dictionary(usps_abbreviations_filename)

# load street names without contractions
print('... loading OpenAddress street names for spell checking')
listStreetWords, listContractions = make_spell_street_name_list(openaddr_filename, dictAddrContra2,
                                                                street_colname=street_colname,
                                                                city_colname=city_colname,
                                                                region_colname=region_colname)
listStreetWords.extend(listContractions)

# checking one letter difference is about 5 x faster than checking two letter difference
#
if spell_check_1_char == True:
    spell1 = SpellChecker(language=None, distance=1)
    spell1.word_frequency.load_words(listStreetWords)
else:
    spell1 = None

if spell_check_2_char == True:
    spell2 = SpellChecker(language=None, distance=2)
    spell2.word_frequency.load_words(listStreetWords)
else:
    spell2 = None

rowCount = 0
goodCount = 0
badCount = 0
listQualityCount = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

print('... loading common street name mis-spellings ...')
if common_misspells_filename != '':
    dictFound = readCommonMisspellsFile(
        common_misspells_filename)  # use this for a heuristic to hold previously found mis-formed street names
else:
    dictFound = {}

# MAIN LOOP
# iterate over the input parking tickets file
#
print('... geocoding ...')
last_time = dt.datetime.now()

total_sec = 0
count_sec = 0

if input_geocode_file_name.split('.')[-1] == 'zip':
    archive = zipfile.ZipFile(input_geocode_file_name, 'r')
    csv_file = csv.reader(archive.read(zip_internal_file_path))
else:
    csv_file = open(input_geocode_file_name, "r")

csvreader = csv.reader(csv_file, delimiter=',', quotechar='"')
for row in csvreader:
    flag_good_row = True

    bad_row_reason = 'Unknown'
    if rowCount == 0:
        str_header = ';'.join(row) + ';lat;lon;matchNumber;matchAddress;quality'
        good_writer.write(str_header + '\n')
        bad_writer.write(str_header + ';bad_reason' + '\n')

    else:
        # check expected integer columns
        for i in range(len(intColids)):

            # replace nulls in file
            if row[intColids[i]] == 'NULL':
                row[intColids[i]] = '-999'

            if flag_good_row == True:
                if int_checker(row[intColids[i]]) == False:
                    flag_good_row = False
                    bad_row_reason = 'Int error: col ' + str(intColids[i]) + ' ' + str(row[intColids[i]])

        # check expected float columns
        for i in range(len(floatColids)):

            # replace nulls in file
            if row[floatColids[i]] == 'NULL':
                row[floatColids[i]] = '-999'

            if flag_good_row == True:
                if float_checker(row[floatColids[i]]) == False:
                    flag_good_row = False
                    bad_row_reason = 'Float error: col ' + str(floatColids[i]) + ' ' + str(row[floatColids[i]])

        # check expected date columns
        for i in range(len(datetimeColids)):
            if flag_good_row == True:
                if datetime_checker(row[datetimeColids[i]], datetimeFormat) == False:
                    flag_good_row = False
                    bad_row_reason = 'Date error: col ' + str(datetimeColids[i]) + ' ' + str(row[datetimeColids[i]])

        if flag_good_row == True:

            # attempt geocode
            address = row[geocode_file_address_colid]
            lat, lon, matchNumber, matchAddress, quality, dictFound = geocode_address(address,
                                                                                      house_number_match_range,
                                                                                      dictFound,
                                                                                      dictAddrContra,
                                                                                      listStreetNames,
                                                                                      dictStreetNames_listNums,
                                                                                      spell1, spell2)
            listQualityCount[quality] += 1

            if quality <= 0:
                flag_good_row = False
                bad_row_reason = 'MM fail'

            strMatch = ';'.join([str(lat), str(lon), str(matchNumber), matchAddress, str(quality)])
            # put quotes around address columns
            row[2] = row[2].replace(';', ' ')
            row[2] = '"' + row[2] + '"'
            row[-1] = '"' + row[-1] + '"'
            strOutput = ';'.join(row) + ';' + strMatch

        if flag_good_row == True:
            good_writer.write(strOutput + '\n')
            goodCount += 1
        else:
            bad_writer.write(';'.join(row) + ';' + bad_row_reason + '\n')
            badCount += 1

    rowCount += 1

    user_feedback_multiple = 100000
    if rowCount % user_feedback_multiple == 0:
        this_time = dt.datetime.now()
        diff_sec = round((this_time - last_time).total_seconds(), 1)
        total_sec += int(diff_sec)
        count_sec += int(1)
        avg_sec = round(user_feedback_multiple / (float(total_sec) / float(count_sec)), 1)
        print(str(this_time)[:19], '...', rowCount, '... good:', goodCount, '...', 'bad:', badCount, '... took:',
              diff_sec, 'secs ... total:', total_sec, 'secs ... avg per sec:', avg_sec)
        last_time = this_time
        strListQualityCount = list(map(str, listQualityCount))
        print('quality outputs:', ', '.join(strListQualityCount))

    if rowCount > max_rows_to_parse:
        break

endtime = dt.datetime.now()
str_endtime = str(endtime)[:19]

diff_sec = abs(round((starttime - endtime).total_seconds(), 0))
print('starttime:', str_starttime, ' ... endtime:', str_endtime, '... total processing time:', str(diff_sec), 'secs')
bad_writer.close()
good_writer.close()

# write common misspells dictionary out to file:
if common_misspells_filename != '':
    writeCommonMisspellsFile(common_misspells_filename, dictFound)

print('Finished!')
